#!/bin/bash

# More details at https://docs.gitlab.com/charts/installation/

helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab gitlab/gitlab -f values-demo.yaml
