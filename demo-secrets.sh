#!/bin/bash
bold=$(tput bold)
normal=$(tput sgr0)
red=$(tput setaf 1)

if [ -z "$EMAIL_PASSWORD" ]; then
    echo ${bold}${red}Set EMAIL_PASSWORD in environment or something, dude.${normal}
    echo Hint: ${bold}EMAIL_PASSWORD=GreatPasswordHere ./demo-secrets.sh${normal}
    exit 1
fi

kubectl create secret generic email-password --from-literal=password=$EMAIL_PASSWORD
